package pages;

import wrappers.Annotations;

public class MyLeadsPage extends Annotations{
	
	public CreateLeadPage ClickCreateLead () {

driver.findElementByLinkText("Create Lead").click();
return new CreateLeadPage();

}
	
	public FindLeadPage ClickFindLeads() {	
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		return new FindLeadPage();
	}
	
	
public MergeLeadPage ClickMergeLead() {
		
		driver.findElementByLinkText("Merge Leads").click();
		return new MergeLeadPage();
	}
}
