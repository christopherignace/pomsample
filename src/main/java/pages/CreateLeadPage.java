package pages;

import wrappers.Annotations;

public class CreateLeadPage extends Annotations {
	
public CreateLeadPage CompanyName(String data) {
		
		driver.findElementById("createLeadForm_companyName").sendKeys(data);
		return this;		
	}

public CreateLeadPage FirstName(String data) {
	
	driver.findElementById("createLeadForm_firstName").sendKeys(data);
	return this;		
}
	
	public CreateLeadPage LastName(String data) {
		
		driver.findElementById("createLeadForm_lastName").sendKeys(data);
		return this;		
	}
	
public ViewLeadPage submit() {
		
	driver.findElementByName("submitButton").click();
		return new ViewLeadPage();		
	}
	

	
}
