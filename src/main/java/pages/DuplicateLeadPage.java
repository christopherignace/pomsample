package pages;

import wrappers.Annotations;

public class DuplicateLeadPage extends Annotations {

	public ViewLeadPage ClickDupLeadSubmit() {
		driver.findElementByXPath("(//input[@class='smallSubmit'])[1]").click();
		return new ViewLeadPage();	
		
	}
}
