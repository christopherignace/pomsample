package pages;

import org.openqa.selenium.Alert;

import wrappers.Annotations;

public class MergeLeadPage extends Annotations {
	
	public MergeLeadPage FromLead() {
		
		driver.findElementById("ComboBox_partyIdFrom").sendKeys("10011");
		return this;		
	}

	public MergeLeadPage ToLead() {
		driver.findElementById("ComboBox_partyIdTo").sendKeys("10012");
		return this;	
	}
	
	public MergeLeadPage ClickMerge() {
		driver.findElementByXPath("//a[text()='Merge']").click();
		return this;		
	}
	
	public ViewLeadPage HandleAlerts() {
	Alert alr1 = driver.switchTo().alert();
	String textbox = driver.switchTo().alert().getText();
	System.out.println("Alert message box content is:" +textbox);
	alr1.accept();
	return new ViewLeadPage();
	}	
}