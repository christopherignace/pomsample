package pages;

import wrappers.Annotations;

public class MyHomePage extends Annotations {

	public MyLeadsPage ClickLeads() {
		
		driver.findElementByLinkText("Leads").click();
		return new MyLeadsPage();
	}
}
