package pages;

import wrappers.Annotations;

public class EditLeadPage extends Annotations {
	
	public EditLeadPage EditCompanyName(String data) {
		driver.findElementById("updateLeadForm_companyName").sendKeys(data);
		return this;
	}
	public EditLeadPage EditFirstName(String data) {
		driver.findElementById("updateLeadForm_firstName").sendKeys(data);
		return this;
	}

	public EditLeadPage EditLastName(String data) {
		driver.findElementById("updateLeadForm_lastName").sendKeys(data);
		return this;
	}
	
	public ViewLeadPage ClickUpdate() {
		driver.findElementByXPath("(//input[@class='smallSubmit'])[1]").click();
		return new ViewLeadPage();
		
	}
	
}