package pages;

import wrappers.Annotations;

public class FindLeadPage extends Annotations {
	
	public FindLeadPage ClickFirstName() {
		
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		return this;
		
	}

	public FindLeadPage SendFirstName() {
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("chris");
		return this;	
	}
	
	public FindLeadPage ClickFindLead() {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		return this;
	}
	
	public ViewLeadPage ClickFirstLeadid() throws InterruptedException {
		Thread.sleep(5000);
	driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
	return new ViewLeadPage();
}
}
