package pages;

import wrappers.Annotations;

public class ViewLeadPage extends Annotations {
	
	public ViewLeadPage verifyFirstName(String data) {
		String FirstName = driver.findElementById("viewLead_firstName_sp").getText();
		if(FirstName.contains(data)) {
			System.out.println("First name is matched");
		}else {
			System.out.println("first name is  mismatching");
		}
		return this;
	}
	
	public EditLeadPage ClickEdit() {
		driver.findElementByXPath("//a[text()='Edit']").click();
		return new EditLeadPage();
	}
	
	public DuplicateLeadPage ClickDuplicate() {		
		driver.findElementByXPath("//a[text()='Duplicate Lead']").click();
		return new DuplicateLeadPage();
	}
	
	public MyLeadsPage ClickDelete() {
		driver.findElementByXPath("//a[text()='Delete']").click();
		return new MyLeadsPage();
	}
}