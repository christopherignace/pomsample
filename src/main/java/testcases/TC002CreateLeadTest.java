package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC002CreateLeadTest extends Annotations {
	
	@BeforeTest
	public void setData() {
		excelFileName = "TC002";
	}
	
	@Test(dataProvider = "fetchData")
	public void CreateLead (String userName, String password, String logInName,String Cname, String Fname, String Lname) {
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLoginButton()
		.verifyLoginName(logInName)
		.Clickcrm()
		.ClickLeads()
		.ClickCreateLead()
		.CompanyName(Cname)
		.FirstName(Fname)
		.LastName(Lname)
		.submit()
		.verifyFirstName(Fname);
		//.clickLogoutButton();
	}
}