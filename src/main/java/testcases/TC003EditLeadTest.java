package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC003EditLeadTest extends Annotations {
	
	
	@BeforeTest
	public void setData() {
		excelFileName = "TC003";
	}
	
	@Test(dataProvider = "fetchData")
	public void EditLead (String userName, String password, String logInName,String CUpdname, String FUpdname, String LUpdname) throws InterruptedException {
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLoginButton()
		.verifyLoginName(logInName)
		.Clickcrm()
		.ClickLeads()
		.ClickFindLeads()
		.ClickFirstName()
		.SendFirstName()
		.ClickFindLead()
		.ClickFirstLeadid()
		.ClickEdit()
		.EditCompanyName(CUpdname)
		.EditFirstName(FUpdname)
		.EditLastName(LUpdname)
		.ClickUpdate();
}
}
