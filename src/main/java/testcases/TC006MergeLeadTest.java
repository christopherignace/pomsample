package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC006MergeLeadTest extends Annotations{
	
	@BeforeTest
	public void setData() {
		excelFileName = "TC001";
	}
		
	@Test(dataProvider = "fetchData")
	public void MergeLead (String userName, String password, String logInName) throws InterruptedException {
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLoginButton()
		.verifyLoginName(logInName)
		.Clickcrm()
		.ClickLeads()
		.ClickMergeLead()
		.FromLead()
		.ToLead()
		.ClickMerge()
		.HandleAlerts();
}
}